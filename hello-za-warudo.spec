Name:            hello-za-warudo
Version:         1.0
Release:         1
Summary:         Behold the power of the world!
License:         GPLv3+
URL:             https://example.com/%{name}
Source0:         https://example.com/%{name}/release/%{name}-%{version}.tar.gz
BuildRequires:   go

%undefine _missing_build_ids_terminate_build

%description
You thought this would be a meme, but it was actually ME, RPM!

%prep
%setup

%build
GOPATH=%{_builddir}/ go mod download
GOPATH=%{_builddir}/ go build -v -o hello-za-warudo

%install
mkdir -p %{buildroot}/usr/bin/
install -m 755 hello-za-warudo %{buildroot}/usr/bin/%{name}

%files
%{_bindir}/%{name}

%changelog
* Sat Jul 18 2020 Brad Stinson <brad@bradthebuilder.me> - 1.0
- First attempt at building za warudo
