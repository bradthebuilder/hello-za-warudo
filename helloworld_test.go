// +unit

package main

import "testing"

func TestZaWarudo(t *testing.T) {
	want := "hello, za warudo!"
	if got := greet(); got != want {
		t.Errorf("main() = %q, want %q", got, want)
	}
}
