package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

func greet() string {
	// Hello, World!
	message := "hello, za warudo!"
	fmt.Printf("%s\n", message)
	return message
}

func main() {
	greet()

	// Open MP3 file
	f, err := os.Open("za-warudo.mp3")
	if err != nil {
		log.Fatal(err)
	}

	// Create streamer from MP3
	streamer, format, err := mp3.Decode(f)
	if err != nil {
		log.Fatal(err)
	}

	// Close streamer and opened file on program exit
	defer streamer.Close()

	// Initialize speaker
	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	// Play streamer and exit program when finished
	done := make(chan bool)
	speaker.Play(beep.Seq(streamer, beep.Callback(func() {
		done <- true
	})))
	<-done
}
